package controller;

import model.Enemy;
import view.Platform;
import model.Character;
import java.util.ArrayList;

public class DrawingLoop implements Runnable {

    private Platform platform;
    private int frameRate;
    private float interval;
    private boolean running;

    public DrawingLoop(Platform platform) {
        this.platform = platform;
        frameRate = 60;
        interval = 1000.0f / frameRate; // 1000 ms = 1 second
        running = true;
    }

    private void checkDrawCollisions(Character mainCharacter,ArrayList<Enemy> enemyList) throws InterruptedException {
        mainCharacter.checkReachGameBorder();

        for (Enemy e : enemyList) {
            if (mainCharacter.getBoundsInParent().intersects(e.getBoundsInParent())) {
                mainCharacter.collided(e);
            }
        }
    }

    private void paint(Character mainCharacter,ArrayList<Enemy> enemyList) throws InterruptedException {
        mainCharacter.repaint(enemyList);
    }

    @Override
    public void run() {
        while (running) {

            float time = System.currentTimeMillis();

            try {
                checkDrawCollisions(platform.getMainCharacter(),platform.getEnemyList());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            try {
                paint(platform.getMainCharacter(),platform.getEnemyList());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            time = System.currentTimeMillis() - time;

            if (time < interval) {
                try {
                    Thread.sleep((long) (interval - time));
                } catch (InterruptedException ignore) {
                }
            } else {
                try {
                    Thread.sleep((long) (interval - (interval % time)));
                } catch (InterruptedException ignore) {
                }
            }
        }
    }
}
