package controller;

import javafx.scene.input.KeyCode;
import model.Direction;
import model.Enemy;
import model.Score;
import model.Character;
import view.Platform;

import java.util.ArrayList;

public class GameLoop implements Runnable {

    private Platform platform;
    private int frameRate;
    private float interval;
    private boolean running;

    public GameLoop(Platform platform) {
        this.platform = platform;
        frameRate = 10;
        interval = 1000.0f / frameRate;
        running = true;
    }

    private void update(Character mainCharacter, ArrayList<Enemy> enemyList) {

        mainCharacter.getCurrentImageView().tick();

        for(Enemy e : enemyList) {
            e.getIdleView().tick();
        }

        if (platform.getKeys().isPressed(KeyCode.LEFT)) {
            mainCharacter.setDirection(Direction.LEFT);
        }

        if (platform.getKeys().isPressed(KeyCode.RIGHT)) {
            mainCharacter.setDirection(Direction.RIGHT);
        }

        if (platform.getKeys().isPressed(KeyCode.UP)) {
            mainCharacter.setDirection(Direction.UP);
        }

        if (platform.getKeys().isPressed(KeyCode.DOWN)) {
            mainCharacter.setDirection(Direction.DOWN);
        }

        if (platform.getKeys().isPressed(KeyCode.UP) && platform.getKeys().isPressed(KeyCode.RIGHT)) {
            mainCharacter.setDirection(Direction.UPRIGHT);
        }

        if (platform.getKeys().isPressed(KeyCode.DOWN) && platform.getKeys().isPressed(KeyCode.RIGHT)) {
            mainCharacter.setDirection(Direction.DOWNRIGHT);
        }

        if (platform.getKeys().isPressed(KeyCode.UP) && platform.getKeys().isPressed(KeyCode.LEFT)) {
            mainCharacter.setDirection(Direction.UPLEFT);
        }

        if (platform.getKeys().isPressed(KeyCode.DOWN) && platform.getKeys().isPressed(KeyCode.LEFT)) {
            mainCharacter.setDirection(Direction.DOWNLEFT);
        }

        if (!platform.getKeys().isPressed(KeyCode.LEFT) && !platform.getKeys().isPressed(KeyCode.RIGHT) && !platform.getKeys().isPressed(KeyCode.UP) && !platform.getKeys().isPressed(KeyCode.DOWN)) {
            mainCharacter.setDirection(Direction.NONE);
        }

        if (mainCharacter.getCurrentImageView().isLooped()) {
            mainCharacter.stopAttack();
        }

        if (platform.getKeys().isPressed(KeyCode.SPACE)) {
            mainCharacter.attack();
        }

        if (platform.getKeys().isPressed(KeyCode.CONTROL)) {
            mainCharacter.superAttack();
        }
    }

    private void updateScore( Character mainCharacter, Score score) {

        javafx.application.Platform.runLater(() -> {
                score.setPoint(mainCharacter.getScore());
        });
    }

    @Override
    public void run() {
        while (running) {

            float time = System.currentTimeMillis();

            update(platform.getMainCharacter(),platform.getEnemyList());
            updateScore(platform.getMainCharacter(),platform.getScore());

            time = System.currentTimeMillis() - time;

            if (time < interval) {
                try {
                    Thread.sleep((long) (interval - time));
                } catch (InterruptedException ignore) {
                }
            } else {
                try {
                    Thread.sleep((long) (interval - (interval % time)));
                } catch (InterruptedException ignore) {
                }
            }
        }
    }
}
