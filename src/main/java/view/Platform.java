package view;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.Pane;
import model.Enemy;
import model.Keys;
import model.Score;
import model.Character;

import java.util.ArrayList;
import java.util.Random;

public class Platform extends Pane {

    public static final int WIDTH = 960;
    public static final int HEIGHT = 540;
    public static final int GROUND = 300;

    private Image platformImg;

    private Character mainCharacter;
    private ArrayList<Enemy> enemyList;
    private Score score;

    private Keys keys;

    public Platform() {
        enemyList = new ArrayList();
        keys = new Keys();
        platformImg = new Image(getClass().getResourceAsStream("/background.jpg"));
        ImageView backgroundImg = new ImageView(platformImg);
        backgroundImg.setFitHeight(HEIGHT);
        backgroundImg.setFitWidth(WIDTH);
        mainCharacter = new Character(50, GROUND+((HEIGHT-GROUND)/2)-50);
        buildEnemyList();
        score = new Score(50,HEIGHT - 50);
        getChildren().add(backgroundImg);
        getChildren().addAll(enemyList);
        getChildren().add(score);
        getChildren().add(mainCharacter);
    }

    private void buildEnemyList() {
        for (int i=0 ; i<5 ; i++) {
            Enemy enemy = new Enemy(mainCharacter);
            enemyList.add(enemy);
        }
    }

    public Character getMainCharacter() { return mainCharacter; }

    public ArrayList<Enemy> getEnemyList() { return enemyList; }

    public Keys getKeys() { return keys; }

    public Score getScore() {return score;}

}

