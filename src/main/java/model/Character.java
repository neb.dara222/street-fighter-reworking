package model;

import javafx.scene.image.Image;
import javafx.scene.layout.Pane;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import view.Platform;

import java.util.ArrayList;

public class Character extends Pane {


    public int cur_width;
    public int cur_height;

    private Image idleImg;
    private Image moveForwardImg;
    private Image moveBackwardImg;
    private Image attackImg;
    private Image superAttackImg;

    private AnimatedSprite idleView;
    private AnimatedSprite moveForwardView;
    private AnimatedSprite moveBackwardView;
    private AnimatedSprite attackView;
    private AnimatedSprite superAttackView;

    private AnimatedSprite currentImageView;

    private int x;
    private int y;
    private int startX;
    private int startY;
    private int velocity = 5;

    private int score = 0;

    private Direction direction;

    boolean isAttack = false;
    boolean isSuperAttack = false;

    ArrayList<Enemy> toKill;

    public Character(int x, int y) {
        this.startX = x;
        this.startY = y;
        this.x = x;
        this.y = y;
        this.setTranslateX(x);
        this.setTranslateY(y);

        this.toKill = new ArrayList<>();

        this.idleImg = new Image(getClass().getResourceAsStream("/CharacterIdle.png"));
        this.moveForwardImg = new Image(getClass().getResourceAsStream("/CharacterMoveForward.png"));
        this.moveBackwardImg = new Image(getClass().getResourceAsStream("/CharacterMoveBackward.png"));
        this.attackImg = new Image(getClass().getResourceAsStream("/CharacterAttack.png"));
        this.superAttackImg = new Image(getClass().getResourceAsStream("/CharacterSuperAttack.png"));

        this.idleView = new AnimatedSprite(idleImg, 4, 4, 128, 66);
        this.moveForwardView = new AnimatedSprite(moveForwardImg, 4, 4, 129, 66);
        this.moveBackwardView = new AnimatedSprite(moveBackwardImg, 2, 2, 128, 66);
        this.attackView = new AnimatedSprite(attackImg, 6, 6, 263, 99);
        this.superAttackView = new AnimatedSprite(superAttackImg, 21, 3, 960, 191);

        setCurrentImageView(idleView);
    }

    public void attack() {
        isSuperAttack = false;
        isAttack = true;
    }

    public void superAttack() {
        isAttack = false;
        isSuperAttack = true;
    }

    public void stopAttack() {
        if (isAttack || isSuperAttack) {
            javafx.application.Platform.runLater(() -> {
                setCurrentImageView(idleView);
            });
        }

        for (Enemy e : toKill) {
            e.respawn(this);
            score++;
        }
        toKill.clear();

        isAttack = false;
        isSuperAttack = false;

    }

    public void checkReachGameBorder() {
        if (x <= 0) {
            x = 0;
        } else if (x + cur_width >= Platform.WIDTH) {
            x = Platform.WIDTH - cur_width;
        }

        if (y <= Platform.GROUND - cur_height) {
            y = Platform.GROUND - cur_height;
        } else if (y >= Platform.HEIGHT - cur_height) {
            y = Platform.HEIGHT - cur_height;
        }
    }


    public void executeAttack() {
        if (isAttack) {
            if (currentImageView != attackView) {
                javafx.application.Platform.runLater(() -> {
                    setCurrentImageView(attackView);
                });
            }
        }
    }

    public void executeSuperAttack(ArrayList<Enemy> enemyArrayList) throws InterruptedException {

        if (isSuperAttack) {
            setTranslateX(0);
            setTranslateY(Platform.GROUND);

            if (currentImageView != superAttackView) {
                javafx.application.Platform.runLater(() -> {
                    setCurrentImageView(superAttackView);
                });
            }

            toKill = new ArrayList(enemyArrayList);

            x = startX;
            y = startY;
        }
    }

    public void move() {
        if (direction.equals(Direction.UP) || direction.equals(Direction.UPLEFT) || direction.equals(Direction.UPRIGHT)) {
            y = y - velocity;
        }

        if(direction.equals(Direction.DOWN)||direction.equals(Direction.DOWNLEFT)||direction.equals(Direction.DOWNRIGHT)) {
            y = y + velocity;
        }

        if (direction.equals(Direction.LEFT) || direction.equals(Direction.UPLEFT) || direction.equals(Direction.DOWNLEFT)) {
            x = x - velocity;
            if (! isAttack && ! isSuperAttack) {
                if (currentImageView != moveBackwardView) {
                    javafx.application.Platform.runLater(() -> {
                        setCurrentImageView(moveBackwardView);
                    });
                }
            }
        }

        if(direction.equals(Direction.RIGHT)||direction.equals(Direction.UPRIGHT)||direction.equals(Direction.DOWNRIGHT)) {
            x = x + velocity;
            if (! isAttack && ! isSuperAttack) {
                if (currentImageView != moveForwardView) {
                    javafx.application.Platform.runLater(() -> {
                        setCurrentImageView(moveForwardView);
                    });
                }
            }
        }

        if(direction.equals(Direction.NONE)) {
            if (!isAttack && !isSuperAttack && this.currentImageView != idleView) {
                javafx.application.Platform.runLater(() -> {
                    setCurrentImageView(idleView);
                });
            }
        }

        setTranslateX(x);
        setTranslateY(y);
    }



    public void repaint(ArrayList<Enemy> enemyList) throws InterruptedException {
        move();
        executeAttack();
        executeSuperAttack(enemyList);
    }

    public void collided(Enemy e) {

        if (isAttack) {
            if(!toKill.contains(e)) toKill.add(e);
        }
    }

    public AnimatedSprite getCurrentImageView() { return currentImageView; }

    public void setCurrentImageView(AnimatedSprite nextCurrentImageView) {

        this.currentImageView = nextCurrentImageView;
        cur_width = nextCurrentImageView.width;
        cur_height = nextCurrentImageView.height;

        this.getChildren().clear();
        this.getChildren().addAll(this.currentImageView);
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getScore() {
        return score;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    public Direction getDirection() {
        return direction;
    }
}
