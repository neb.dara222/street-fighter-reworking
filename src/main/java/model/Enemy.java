package model;

import javafx.scene.image.Image;
import javafx.scene.layout.Pane;
import view.Platform;

import java.util.Random;

public class Enemy extends Pane {

    public int cur_width ;
    public int cur_height;

    private Image idleImg;

    private AnimatedSprite idleView;

    public Enemy(Character mainCharacter) {
        this.idleImg = new Image(getClass().getResourceAsStream("/StrawPuppet.png"));
        this.idleView = new AnimatedSprite(idleImg, 6, 6,91,89);
        cur_width = idleView.width;
        cur_height = idleView.height;
        this.getChildren().add(this.idleView);
        respawn(mainCharacter);
    }

    public void respawn(Character mainCharacter) {
        Random rn = new Random();
        int rand_x = mainCharacter.getX();
        int rand_y = mainCharacter.getY();
        while ( rand_x >= mainCharacter.getX()-100 && rand_x <= mainCharacter.getX()+100 && rand_y >= mainCharacter.getY()-100 && rand_y <= mainCharacter.getY()+100) {
            rand_x = rn.nextInt(Platform.WIDTH - cur_width + 1);
            rand_y = Platform.GROUND + rn.nextInt(Platform.HEIGHT - Platform.GROUND - cur_height + 1);
        }

        this.setTranslateX(rand_x);
        this.setTranslateY(rand_y);
    }

    public AnimatedSprite getIdleView() {
        return idleView;
    }

}
